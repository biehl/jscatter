# Advise GitLab that these environment vars should be loaded from the Variables config.
variables:
    PYPI_USER: SECURE
    PYPI_PASSWORD: SECURE

stages:
  - pretest
  - test
  - build
  - deploy
  - cleanup

before_script:
  - export DEBIAN_FRONTEND=noninteractive
  - apt-get update -qq && apt-get -y -qq install apt-utils
  - echo "Europe/Paris" > /etc/timezone
  - apt-get install -y gfortran grace

defaulttest: # default test, done in any case in the beginning to check and avoid other tests if it fails
  image:
    name: python:3.13  # on debian latest
  stage: pretest
  script:
    - python3 -m venv civenv
    - source civenv/bin/activate
    - python3 -m pip install $(pwd)
    # test if all files are present in this way
    #- python3 -c "import jscatter;jscatter.test.doTest()"
    - python3 -m unittest jscatter.test

upgradetest:   # test if in rolling ubuntu and after upgrade all works including pymatgen
  image: ubuntu:rolling
  stage: test
  needs: ["defaulttest"]
  script:
    - apt-get install -y python3-dev python3-pip python3-venv
    - python3 -m venv civenv
    - source civenv/bin/activate
    - python3 -m pip install pymatgen
    - python3 -m pip install $(pwd)
    - python3 -m unittest jscatter.test
    - python3 -m unittest jscatter.libs.cubature.test_cubature
  only:
    - dev

pythonoldest:   # python the oldest supported version on debian
  image: python:3.9
  stage: test
  needs: ["defaulttest"]
  script:
    - python3 -m venv civenv
    - source civenv/bin/activate
    - python3 -m pip install $(pwd)
    - python3 -m unittest jscatter.test
  only:
    - dev

pages:  
  # create pages on iffgit only for internal usage in dev
  image: python:3.12
  stage: test
  needs: ["defaulttest"]
  script:
    - python3 -m venv civenv
    - source civenv/bin/activate
    #- apt-get install -y texlive-latex-extra
    - python3 -m pip install -r readthedocs-requirements.txt
    - python3 -m pip install $(pwd)
    - sphinx-build -b html src/jscatter/doc/source src/jscatter/doc/html
    - mv src/jscatter/doc/html/ public/
  artifacts:
    paths:
    - public
    expire_in: 4 week
  only:
  - dev

#deploy_gitlab: # done over mirroring in settings
#  image: python:3.12
#  stage: test
#  needs: ["defaulttest"]
#  script:   # Configure the PyGitlab credentials, then push the package, and cleanup the credentials.
#    - apt-get install -y git
#    - git config --global user.name "${GITLAB_USER}"
#    - git config --global user.email "${GITLAB_USER_EMAIL}"
#    - git push https://${GITLAB_USER}:${GITLAB_Token}@gitlab.com/biehl/jscatter.git HEAD:master
#  only:
#    - master

buildunix:
  image: python:3.12
  stage: build
  needs: ["defaulttest"]
  script:
    - python3 -m venv civenv
    - source civenv/bin/activate
    - python3 -m pip install build cibuildwheel
    - python3 -m build -s --outdir wheelhouse
  artifacts:
    paths:
      - wheelhouse/
    expire_in: 1 week
  only:
    - master

deploy_pypi:
  image: python:3.12
  stage: deploy
  needs: ["buildunix"]
  script:   # Configure the PyPI credentials, then push the package, and cleanup the credentials.
    - python3 -m pip install twine
    - echo "[distutils]" >> ~/.pypirc
    - echo "index-servers =" >> ~/.pypirc
    - echo "    pypi" >> ~/.pypirc
    - echo " " >> ~/.pypirc
    - echo "[pypi]" >> ~/.pypirc
    - echo "username=" ${PYPI_USER} >> ~/.pypirc
    - echo "password=" ${PYPI_PASSWORD} >> ~/.pypirc
    - twine upload wheelhouse/*  # This will fail if version is already there
    - echo "" > ~/.pypirc && rm ~/.pypirc  # If the above fails, this won't run.
  artifacts:
    paths:
      - wheelhouse/
  only:
    - master

cleanup_pypirc:
   image: python:3.12
   stage: cleanup
   needs: ["deploy_pypi"]
   #when: always   # this is important; run even if preceding stages failed.
   only:
    - master
   script:
    - rm -vf ~/.pypirc  # we don't want to leave these around, but GitLab may clean up anyway.