# Jscatter changelog

The latest version of this file can be 
found at the master branch of the Jscatter repository.

1.7.0
    - updated normal modes in bio, added fake modes
    - support python3.13
    - added flowerlikeMicelle
    - added frequency2timeFF (unift)

1.6.0
    - changed from depreceated distutils to cmake
    - changed coil formfactor amplitude to root of Debye
    - added polygon formfactor
    - added diffuseLamellarStack
    - added Perrin friction factor
    - added support for M3 Mac processors
    - added fixedFiniteZimm and fixedFiniteRouse
    - added DsoverDo for hard spheres
    - added bicelle

1.5.0
    - added biomacromolecules
    - added Bayesian inference for fitting
    - added bicelle hydrodynamic radius
    - added triaxial Ellipsoid
    - added prior to bayes
    - added fjc structure factor
    - added raftDecoratedCoreShell + dropDecoratdCoreshell with added polydispersity

1.4.0
    - added excluded volume to ringPolymer
    - added idealHelix
    - added alternatingCoPolymer
    - added powerLaw
    - use fabio in sasImage to read Xray detector images as edf or cbf and more
    - added filter in sas.lineAverage
    - added anisotropic Debye Waller factors in latticeStructureFactor

1.3.0
    - added decoratedCoreShell
    - added fuzzyCylinder
    - added inhomogeneousSphere
    - added randomLattice
    - added fractal structure factor
    - added Schulz-Zimm distribution
    - added showlastErrPlot2D
    - added headless mode for cluster usage
    - added imagehash
    - added persistent columnIndex through write/reread data
    - added inhomogeneousCylinder
    - added Two Yukawa structure factor
    - added prism formfactor
    - added multidimensional quadrature (cubature)
    - added ornsteinZernike
    - added orientedCloudScattering
    - added polymerCorLength
    - added DAB

1.2.0
    - removed python 2.7 compatibility and changes for Python3
    - smear as decorator to automatically extend over edges
    - smear handles beamProfiles in data for fitting with different smearing in data
    - added weakPolyelectrolyte
    - added multiParDistributedAverage
    - cloudscattering allows different formfactors
    - added linearPearls

1.1.0
    - added citation reference to Jscatter paper in PlosOne
    - added offset detector calibration for sasImage
    - azimuthal average in sasImage
    - lattice from CIF files with atomic coordinates

1.0.0
    - stable release

0.9.5
    - changes for Windows compatibility

0.9.4
 - changes for numpy==1.16 compatibility
 - added installation description for Windows Subsystem for Linux
 - added Ipython Notebooks and direct link to myBinder as live demo

0.9.3
 - added guinierPorod3d guinierPorod

0.9.2
 - added radial interpolation to sasImage.asdataArray
 - improved  getBeamWidth and desmear
 - added more test
 - changes from inspection
 - orientedLatticeStructureFactor now with OpenMP in Fortran
 - added example_2DFitting_CrystalPeaks

0.9.1
 - added Changelog


|
|
|
|
|
|
|
|
|
|
|




