mpl
===

.. automodule:: jscatter.mpl
    :noindex:


.. currentmodule:: jscatter.mpl

Matplotlib figures
------------------
.. autosummary::
    mplot
    surface
    scatter3d
    contourImage
    plot2Dimage
    showColors
    contourOnCube
    showlastErrPlot2D
    test

Figure Methods
--------------

.. autosummary::
   ~jsFigure.Multi
   ~jsFigure.Addsubplot
   ~jsFigure.Plot
   ~jsFigure.Clear
   ~jsFigure.Save
   ~jsFigure.is_open
   ~jsFigure.Exit

Axes Methods
------------

.. autosummary::
    ~jspaperAxes.Plot
    ~jspaperAxes.Yaxis
    ~jspaperAxes.Xaxis
    ~jspaperAxes.Legend
    ~jspaperAxes.Title
    ~jspaperAxes.Subtitle
    ~jspaperAxes.SetView
    ~jspaperAxes.Clear
    ~jspaperAxes.Text
    ~jspaperAxes.Arrow

====

.. automodule:: jscatter.mpl
    :members:
    :undoc-members:
    :show-inheritance:

.. autoclass:: jscatter.mpl.jsFigure
    :noindex:

.. autoclass:: jscatter.mpl.jspaperAxes
    :noindex:




