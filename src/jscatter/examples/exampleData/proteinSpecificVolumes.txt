# Tsai The packing density in proteins: Standard radii and volumes JMB 290, 253 (1999)
# Tsai, Taylor, Chothia, Gerstein, M. (1999).
# The packing density in proteins: Standard radii and volumes.
# Journal of Molecular Biology, 290(1), 253–266. https://doi.org/10.1006/jmbi.1999.2829
Alchohol_dehydrogenase	 1lde      0.750  Tsai
Carbonic_anhydrase_B 2cab          0.729  Tsai
Carboxypeptidase_A 2ctb            0.733  Tsai
Chymotrypsinogen 2cga              0.732  Tsai
Concanavalin_A  1scs               0.732  Tsai
Elastase  1lvy                     0.730  Tsai
Hemoglobin  1mhb                   0.750  Tsai
Lysozyme  8lyz                     0.712  Tsai
Malate_dehydrogenase 	1mld  	   0.742  Tsai
Ribonuclease_A 	1xps               0.703  Tsai
Subtilisin	 1sbt                  0.731  Tsai
Superoxide_dismutase 1sda          0.729  Tsai

# Murphy, Matubayasi, Payne, Levy,(1998).
# Protein hydration and unfolding – insights from experimental partial specific volumes and unfolded protein models.
# Folding and Design, 3(2), 105–118. https://doi.org/10.1016/s1359-0278(98)00016-9
Insulin 9ins                       0.735 Murphy
Pti 4pti                           0.718 Murphy
α-Lactalbumin 1alc                 0.735 Murphy
RibonucleaseA 3rn3                 0.703 Murphy
Lysozyme 1lzt                      0.703 Murphy
Myoglobin 5mbn                     0.745 Murphy
Adenylate_kinase 3adk              0.74  Murphy
Papain 1ppn                        0.719 Murphy
Bence–Jones_REI 1rei               0.726 Murphy
Concanavalin_A 2cna                0.732 Murphy
Elastase 3est                      0.73 Murphy
Carbonic_anhydrase_B 2cab          0.729 Murphy
Subtilisin 2sbt                    0.731 Murphy
# Rhodanese 1rhd                   0.742 Murphy
Carboxypeptidase_A 2ctb            0.733 Murphy

# PARTIAL SPECIFIC VOLUME CHANGES OF PROTEINS DENSIMETRIC STUDIES
# Durchschlag et al BIOCHEMICAL AND BIOPHYSICAL RESEARCH COMMUNICATIONS Vol. 108, No. 3, 1982
# 10.1002/macp.1973.021700115
# Protein partial specific volume depends on temperature, and pH
# dphi/dT = 4.5e-4 ml/g/K
BSA 3V03  0.734 Durchschlag # T=20

# Pilz Czerwenka Macromolecular Chemistry and Physics 1973 DOI:10.1002/MACP.1973.021700115
Lysozyme 8lyz 0.735 Pilz   T=20
Pepsin 4PEP   0.765 Pilz  T=20

# Hydration of proteins and polypeptides.
# Kuntz ID Jr, Kauzmann W. Adv Protein Chem. 1974;28:239-345.
# Protein     pdb                       spezVol author    type         Ref
Ribonuclease 3rn3                       0.692 Kuntz   bovine   10.1016/0006-3002(62)91038-7
CytochromeC  1J3S                       0.720 Kuntz   human    10.1016/S0065-3233(08)60128-X
Lysozyme    1DPX                        0.703 Kuntz   henegg   J. Biol. Chem. 1962 237: 1107
Myoglobin   3rgk                        0.743 Kuntz   human    J. Mol. Biol. 213 215-8 (1990)
b-Lactoglobulin_monomer 3blg            0.751 Kuntz   bovine   10.1042/bj0300961
Chymotrypsinogen   2cga                 0.720 Kuntz   bovine   J. Biol. Chem. 1951 190: 799-.
Thrombin   3PMB                         0.690 Kuntz   bovine   J. Biol. Chem. 1962 237: 3074-.
Carboxypeptidase 5CPA                   0.723 Kuntz   bovine   http://dx.doi.org/10.1016/S0065-3233(08)60278-8
# b-LactoglobulinDimmer  3blg           0.751 Kuntz   bovine   10.1042/bj0300961
Ovalbumin 1ova                          0.748 Kuntz   henegg
Bovineserumalbumin 3V03                 0.734 Kuntz   bovine         10.1021/ja01130a018


